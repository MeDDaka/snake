let map = [[],[]];

class Snake {

    constructor(length,x,y){
        this.length = length;
        this.vector = 0;
        this.idTimer = setTimeout(move,1000);
        this.bodySnake = [];
    }

    move = () => {
        switch (this.vector) {
            case 1:
                //right
                this.x++;
                break;
            case 2:
                //left
                this.x--;
                break;
            case 3: 
                //up
                this.y--;
                break;
            case 4: 
                //down
                this.y++;
                break;    
            default:
                break;
        }
    }

    keyPress = (vector) =>{
        this.vector = vector;
    }

}

class Coord {
    constructor (x,y){
        this.x = x;
        this.y = y;
    }
}

let snake = new Snake(3,10,10);

var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");
ctx.beginPath();
ctx.rect(0, 0, 400, 400);
ctx.stroke();

const Draw = () => {

}